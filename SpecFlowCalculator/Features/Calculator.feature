﻿Feature: Calculator
![Calculator](https://specflow.org/wp-content/uploads/2020/09/calculator.png)
This Calculator, can Add numbers.
				 can Subtract Numbers.
				 can Multiply Numbers.
				 can Divide two Numbers.

Link to a feature: [Calculator](SpecFlowCalculator/Features/Calculator.feature)
***Further read***: **[Learn more about how to generate Living Documentation](https://docs.specflow.org/projects/specflow-livingdoc/en/latest/LivingDocGenerator/Generating-Documentation.html)**


Scenario: Addition of two numbers
	Given the inputs are 50, 50
	When the two numbers are added
	Then the result should be 100

Scenario: Subtraction of numbers
	Given the inputs are 50, 25
	When the first number is subtracted with the second input
	Then the result should be 25

Scenario: Multiplication of numbers
	Given the inputs are 2, 2
	When the numbers are multiplied with eachother
	Then the result should be 4

Scenario: Division of numbers
	Given the inputs are 10, 5
	When the numerator is divided using the denominator
	Then the result should be 2