﻿using CalulatorLibrary;
using TechTalk.SpecFlow;
using FluentAssertions;

namespace SpecFlowCalculator.Steps
{
    [Binding]
    public sealed class CalculatorStepDefinitions
    {

        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;
        private readonly Calculator _calculator = new Calculator();
        private int _firstNumber;
        private int _secondNumber;
        private double _result;
        public CalculatorStepDefinitions() { }
        public CalculatorStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }




        [When("the two numbers are added")]
        public void WhenTheTwoNumbersAreAdded()
        {
            //TODO: implement act (action) logic

            this._calculator.Add(this._firstNumber, this._secondNumber);
            this._result = this._calculator.GetResult();
        }

        
        [Given(@"the inputs are (.*), (.*)")]
        public void GivenTheInputsAre(int p0,int p1)
        {
            this._firstNumber = p0;
            this._secondNumber = p1;
        }

        [When(@"the first number is subtracted with the second input")]
        public void WhenTheFirstNumberIsSubtractedWithTheSecondInput()
        {
            this._result = this._firstNumber - this._secondNumber;
        }

        [When(@"the numbers are multiplied with eachother")]
        public void WhenTheNumbersAreMultipliedWithEachother()
        {
            this._result = this._firstNumber * this._secondNumber;
        }

        [When(@"the numerator is divided using the denominator")]
        public void WhenTheNumeratorIsDividedUsingTheDenominator()
        {
            this._result = this._firstNumber / this._secondNumber;
        }




        [Then("the result should be (.*)")]
        public void ThenTheResultShouldBe(int result)
        {
            //TODO: implement assert (verification) logic

            this._result.Should().Be(result);
        }
    }
}
